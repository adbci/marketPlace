<?php

class ProductsCest
{
    protected $token;
    protected $lastBookId;

    public function authentication(ApiTester $I)
    {
        $I->amHttpAuthenticated('satz', '000000Ab');
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendPOST('/api/token');
        $this->token = $I->grabDataFromResponseByJsonPath('$.token');
    }

    public function listingBooks(ApiTester $I, $scenario)
    {
        $I = new ApiTester($scenario);
        $I->wantTo('List all books');

        $I->amBearerAuthenticated($this->token[0]);
        $I->sendGET('/api/books');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();

    }

    public function listingBookById(ApiTester $I, $scenario)
    {
        $I = new ApiTester($scenario);
        $I->wantTo('List book by Id');

        $I->amBearerAuthenticated($this->token[0]);
        $I->sendGET('/api/books/4');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            "id" => 4,
            "name" => "Foo Bar",
            "price" => "19.99"
        ]);

    }

    public function addingBook(ApiTester $I, $scenario)
    {
        $I = new ApiTester($scenario);
        $I->wantTo('Add a book');

        $data = array
        (
            "name" => "Book name Test",
            "price" => "12"
        );

        $I->amBearerAuthenticated($this->token[0]);
        $I->sendPOST('/api/books', $data);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $this->lastBookId = $I->grabDataFromResponseByJsonPath('$id')[0];

    }

    public function updateBookById(ApiTester $I, $scenario)
    {
        $I = new ApiTester($scenario);
        $I->wantTo('Update book by Id');

        $data = array
        (
            "name" => "Updated book name Test",
            "price" => "14"
        );

        codecept_debug($this->lastBookId);

        $I->amBearerAuthenticated($this->token[0]);
        $I->sendPUT('/api/books/' . $this->lastBookId, $data);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            "id" => $this->lastBookId,
            "name" => "Updated book name Test",
            "price" => "14"
        ]);

    }

    public function deleteBookById(ApiTester $I, $scenario)
    {
        $I = new ApiTester($scenario);
        $I->wantTo('Delete book by Id');

        $I->amBearerAuthenticated($this->token[0]);
        $I->sendDELETE('/api/books/' . $this->lastBookId);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();

    }

}
