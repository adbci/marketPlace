<?php

class LoginCest
{
    protected $token;

    public function authentication(ApiTester $I)
    {
        $I->amHttpAuthenticated('satz', '000000Ab');
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendPOST('/api/token');
        $this->token = $I->grabDataFromResponseByJsonPath('$.token');
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function testLogin(ApiTester $I, $scenario)
    {
        $I = new ApiTester($scenario);
        $I->wantTo('Login to application');

        $I->amHttpAuthenticated('satz', '000000Ab');
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendPOST('/api/token');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200

    }

    public function testRegistration(ApiTester $I, $scenario)
    {
        $I = new ApiTester($scenario);
        $I->wantTo('Register into application');

        $data = array
        (
            "name" => "nameTest",
            "surname" => "surnameTest",
            "username" => $this->generateRandomString(),
            "email" => $this->generateRandomString()."@mail.com",
            "password" => "000000Ab",
            "photoUrl" => "test.jpg",
        );

        $I->sendPOST('/api/register', $data);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();

    }

    public function testAuthentication(ApiTester $I, $scenario)
    {
        $I = new ApiTester($scenario);
        $I->wantTo('Test authentication displaying protected resources');

        $I->amBearerAuthenticated($this->token[0]);
        $I->sendGET('/api/books');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();

    }

}
