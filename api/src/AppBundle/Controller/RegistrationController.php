<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpKernel\Exception\HttpException;
use AppBundle\Form\UserType;
use AppBundle\Form\ChangePasswordType;
use AppBundle\Entity\User;
use AppBundle\Utils\PasswordGenerator;

class RegistrationController extends FOSRestController
{

    /**
     * @ApiDoc(
     *    description="Création compte utilisateur",
     *    output= { "class"=User::class, "collection"=false, "groups"={"user"} },
     *    input= { "class"=User::class }
     * )
     *
     *
     * Dans Postman, ajouter dans le body de la requete
     *
     * {
     * "name":"bucci",
     * "surname":"adrien",
     * "username":"adrien",
     * "email":"adrien.bci@gmail.com",
     * "password":"000000",
     * "photoUrl":"satz",
     * }
     *
     * @Route(path="/api/register", name="registration")
     * @Method("POST")
     */
    public function postRegisterAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $user->setRoles(['ROLE_USER']);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return new JsonResponse(['status' => 'ok']);
        }

        throw new HttpException(400, "Invalid data");
    }
}
