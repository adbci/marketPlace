<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\User;

class LoginController extends Controller
{

    /**
     *
     * @ApiDoc(
     *    description="Retourne un token si login utilisateur correct",
     *    output= { "class"=User::class, "collection"=false, "groups"={"user"} },
     *    input= { "class"=User::class }
     * )
     *
     * Dans Postman
     * Renseigner username et password dans Authorization > Basic Auth
     *
     * @Route("/api/token", name="token_authentication")
     * @Method("POST")
     */
    public function newTokenAction(Request $request)
    {

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['username' => $request->getUser()]);

        if (!$user) {
            throw $this->createNotFoundException();
        }

        $isValid = $this->get('security.password_encoder')
            ->isPasswordValid($user, $request->getPassword());

        if (!$isValid) {
            throw new BadCredentialsException();
        }

        $token = $this->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $user->getUsername(),
                'exp' => time() + 3600000 // 1 hour expiration
            ]);

        return new JsonResponse([
            'token' => $token,
            'name' => $user->getName(),
            'surname' => $user->getSurname(),
            'username' => $user->getUsername(),
            'email' => $user->getEmail(),
            'photoUrl' => $user->getPhotoUrl(),
            'roles' => $user->getRoles()
        ]);
    }
}
