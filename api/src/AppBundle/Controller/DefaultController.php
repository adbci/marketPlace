<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * @ApiDoc(
     *    description="Homepage API",
     * )
     *
     * @Route("/", name="homepage")
     * @Method({"GET"})
     */
    public function indexAction(Request $request)
    {
        $data = [
            'API' => 'Welcome to the marketplace API. Doc available @ http://localhost:8000/documentation'
        ];

        return new JsonResponse($data);
    }

    /**
     * @ApiDoc(
     *    description="Test du token utilisateur",
     *    output= { "class"=User::class, "collection"=false, "groups"={"user"} },
     * )
     *
     * Dans Postman
     * Renseigner username et password dans Authorization > Basic Auth
     *
     * @Route(path="/api/secure", name="secure")
     * @Method({"POST"})
     */
    public function secureUserAction()
    {
        $data = [
            'user' => 'test token OK'
        ];

        return new JsonResponse($data);
    }

}
