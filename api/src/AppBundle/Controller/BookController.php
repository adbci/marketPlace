<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Entity\Book;
use AppBundle\Form\BookType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


class BookController extends FOSRestController
{
    /**
     *
     * @ApiDoc(
     *    description="Récupère la liste des livres",
     *    output= { "class"=Book::class, "collection"=true, "groups"={"book"} }
     * )
     */
    public function getBooksAction()
    {
        $em = $this->getDoctrine()->getManager();
        $books = $em->getRepository(Book::class)->findAll();

        if (!$books) {
            throw new HttpException(400, "Invalid data");
        }

        return $books;
    }

    /**
     *
     * @ApiDoc(
     *    description="Récupère un livre par son id",
     *    output= { "class"=Book::class, "collection"=true, "groups"={"book"} },
     *     input= {"class"=Book::class}
     * )
     */
    public function getBookAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository(Book::class)->find($id);

        if (!$id) {
            throw new HttpException(400, "Invalid id");
        }

        return $book;
    }

    /**
     *
     * @ApiDoc(
     *    description="Ajoute un livre à la collection",
     *    output= { "class"=Book::class, "collection"=true, "groups"={"book"} },
     *     input= {"class"=Book::class}
     * )
     */
    public function postBookAction(Request $request)
    {
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($book);
            $em->flush();

            return $book;
        }

        throw new HttpException(400, "Invalid data");
    }

    /**
     *
     * @ApiDoc(
     *    description="Met un livre à jour depuis son id",
     *    output= { "class"=Book::class, "collection"=true, "groups"={"book"} },
     *     input= {"class"=Book::class}
     * )
     */
    public function putBookAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository(Book::class)->find($id);

        $form = $this->createForm(BookType::class, $book, ['method' => 'PUT']);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($book);
            $em->flush();

            return $book;
        }

        throw new HttpException(400, "Invalid data");
    }

    /**
     *
     * @ApiDoc(
     *    description="Supprime un livre de la collection depuis son id",
     *    output= { "class"=Book::class, "collection"=true, "groups"={"book"} },
     *     input= {"class"=Book::class}
     * )
     */
    public function deleteBookAction($id)
    {
        if (!$id) {
            throw new HttpException(400, "Invalid id");
        }

        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository(Book::class)->find($id);
        $em->remove($book);
        $em->flush();

        return $book;
    }
}
