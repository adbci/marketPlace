<?php

namespace AppBundle\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

class AuthenticationListener
{
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $event->setData([
            'code' => $event->getResponse()->getStatusCode(),
            'payload' => $event->getData(),
        ]);
    }

    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        $event->setData([
            'code' => $event->getResponse()->getStatusCode(),
            'payload' => $event->getData(),
        ]);
    }
}
