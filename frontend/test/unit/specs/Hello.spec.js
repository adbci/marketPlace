import Vue from 'vue'
import VueResource from 'vue-resource'
import Hello from '@/components/demoTests/Hello'

Vue.use(VueResource)

describe('Hello.vue', () => {
  let vm

  beforeEach(function () {
    const Constructor = Vue.extend(Hello)
    vm = new Constructor().$mount()
  })

  it('should check that msg is "Welcome to Your Vue.js App"', () => {
    expect(vm.$data.msg).to.equal('Welcome to Your Vue.js App')
  })

  it('should render correct contents', () => {
    // Given
    const data = {
      data: {
        msg: 'plop'
      }
    }
    const Constructor = Vue.extend(Hello)

    // When
    vm = new Constructor(data).$mount()

    // Then
    expect(vm.$el.querySelector('#hello h1').textContent)
      .to.equal('plop')
  })

  it('should create a counter with zero value', () => {
    expect(vm.$data.counter).to.equal(0)
  })

  it('should render counter with counter data value', () => {
    // Given
    const data = {
      data: {
        counter: 48
      }
    }
    const Constructor = Vue.extend(Hello)

    // When
    vm = new Constructor(data).$mount()

    // Then
    expect(vm.$el.querySelector('#hello div.counter').textContent)
      .to.equal('48')
  })

  it('should check the name of my vue', () => {
    expect(vm.$options.name).to.equal('hello')
  })

  it('should include a Button', () => {
    const clickButton = vm.$options.components.clickButton
    expect(clickButton).to.contain(clickButton)
  })

  it('should define a message to put inside the clickButton', () => {
    expect(vm.$options.components.clickButton.props).to.haveOwnProperty('message')
  })

  it('should verify textContent of the clickButton', () => {
    expect(vm.$el.querySelector('button').textContent)
      .to.equal('Coucou')
  })

  describe('incrementCounter', function () {
    it('should increment the counter to 1', () => {
      // When
      vm.incrementCounter()

      // Then
      expect(vm.$data.counter).to.equal(1)
    })

    it('should increment counter when button from ClickButton is clicked', () => {
      // given
      let button = vm.$el.querySelector('button')

      // when
      button.click()

      // then
      expect(vm.$data.counter).to.equal(1)
    })
  })

  describe('incrementFromTheDice()', () => {
    it('should call api to get the dice number', () => {
      // given
      sinon.stub(Vue.http, 'get').returnsPromise()

      // construct vue
      const Constructor = Vue.extend(Hello)
      const vm = new Constructor().$mount()

      // when
      vm.incrementFromTheDice()

      // then
      expect(Vue.http.get).to.have.been.calledWith('http://setgetgo.com/rollthedice/get.php')

      // after
      Vue.http.get.restore()
    })

    it('should call increment counter from API answer', () => {
      // given
      const promiseCall = sinon.stub(Vue.http, 'get').returnsPromise()
      promiseCall.resolves({body: '5'})

      // construct vue
      const Constructor = Vue.extend(Hello)
      const vm = new Constructor({data: {counter: 6}}).$mount()

      // when
      vm.incrementFromTheDice()

      // then
      expect(vm.$data.counter).to.equal(11)

      // after
      Vue.http.get.restore()
    })

    it('should reinit counter when api rejects error', () => {
      // given
      const promiseCall = sinon.stub(Vue.http, 'get').returnsPromise()
      promiseCall.rejects()

      // construct vue
      const Constructor = Vue.extend(Hello)
      const vm = new Constructor({data: {counter: 6}}).$mount()

      // when
      vm.incrementFromTheDice()

      // then
      expect(vm.$data.counter).to.equal(0)

      // after
      Vue.http.get.restore()
    })

    it('should incrementFromTheDice when button roll-the-dice is clicked', () => {
      // given
      let button = vm.$el.querySelector('button.roll-the-dice')
      const promiseCall = sinon.stub(Vue.http, 'get').returnsPromise()
      promiseCall.resolves({body: '5'})

      // when
      button.click()

      // then
      expect(vm.$data.counter).to.equal(5)

      // after
      Vue.http.get.restore()
    })
  })
})
