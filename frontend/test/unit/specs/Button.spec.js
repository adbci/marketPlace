import Vue from 'vue'
import clickButton from '@/components/demoTests/Button'

describe('Button.vue', () => {
  let vm

  beforeEach(function () {
    // Given
    const config = {
      propsData: {
        message: 'Click Me'
      }
    }

    // When
    const Constructor = Vue.extend(clickButton)

    // Then
    vm = new Constructor(config).$mount()
  })

  it('should check the name of my vue', () => {
    expect(vm.$options.name).to.equal('clickButton')
  })

  it('should render button with text Click Me', () => {
    expect(vm.$el.querySelector('button').textContent)
      .to.equal('Click Me')
  })

  describe('onButtonClick', function () {
    it('should emit click ', () => {
      // Given
      sinon.spy(vm, '$emit')

      // When
      vm.onButtonClick()

      // Then
      expect(vm.$emit).to.have.been.calledWith('buttonHasBeenClicked')
    })
  })

  it('should emit an event when button is clicked', () => {
    // given
    sinon.spy(vm, '$emit')
    const button = vm.$el.querySelector('button')

    // when
    button.click()

    // then
    expect(vm.$emit).to.have.been.calledWith('buttonHasBeenClicked')
  })
})
