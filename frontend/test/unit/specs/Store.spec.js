import store from '../../../src/store/index'
// import * as utils from '../utils/utils'

// const initialState = utils.deepCopy(store.state)
//
// const refreshStore = initialState => {
//   let applyState = utils.deepCopy(initialState)
// console.log(store)
// console.log(store._actions)
// store._actions.refresh(applyState)
// }

describe('Store setting suite verification', () => {
  // afterEach(() => refreshStore(initialState))

  it('is configured correctly', () => {
    expect(true).to.equal(true)
  })
  it('has a valid store object', () => {
    expect(store.getters.serverAPIurl).to.be.equal('http://localhost:8000')
  })
  it('can launch actions', () => {
    store.commit('SET_USER', {'username': 'tatz', 'name': 'Tatz'})
    expect(store.state.user.user.username).to.be.equal('tatz')
    store.commit('SET_USER', null)
    expect(store.state.user.user).to.be.equal(null)
  })
})
