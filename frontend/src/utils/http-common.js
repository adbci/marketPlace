import axios from 'axios'

let serverAPIUrl = 'http://localhost:8000'

export const HTTP = axios.create({
  baseURL: serverAPIUrl
})
