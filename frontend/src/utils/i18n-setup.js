import Vue from 'vue'
import VueI18n from 'vue-i18n'
import axios from 'axios'

Vue.use(VueI18n)

export const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: {
    en: require('../lang/en.json')
  }
})

const loadedLanguages = ['en']

const setI18nLanguage = (lang) => {
  i18n.locale = lang
  axios.defaults.headers.common['Accept-Language'] = lang
  document.querySelector('html').setAttribute('lang', lang)
  return lang
}

export const loadLanguageAsync = (lang) => {
  if (i18n.locale !== lang) {
    if (!loadedLanguages.find((l) => l === lang)) {
      return axios.get(`/static/lang/${lang}.json`)
        .then((response) => {
          i18n.setLocaleMessage(lang, response.data)
          loadedLanguages.push(lang)
          return setI18nLanguage(lang)
        })
    }

    return Promise.resolve(setI18nLanguage(lang))
  }
  return Promise.resolve(lang)
}
