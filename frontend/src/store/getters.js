export default {
  loading (state) {
    return state.loading
  },
  error (state) {
    return state.error
  },
  serverURI (state) {
    return state.serverURI
  },
  serverAPIurl (state) {
    return state.serverAPIurl
  }

}
