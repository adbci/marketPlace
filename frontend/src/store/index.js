import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user/index'
import state from './state'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  getters: getters,
  mutations: mutations,
  actions: actions,
  state: state,
  modules: {
    user: user
  },
  strict: debug
})
