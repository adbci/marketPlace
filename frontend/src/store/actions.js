export default {
  refresh ({commi}) {
    this.commit('REFRESH', null)
  },
  logout ({commit}) {
    this.commit('SET_USER', null)
    this.commit('SET_TOKEN', null)
  }
}
