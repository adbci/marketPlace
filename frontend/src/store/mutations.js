export default {
  SET_ERROR (state, payload) {
    state.error = payload
  },
  SET_LOADING (state, payload) {
    state.loading = payload
  },
  REFRESH (state, initialState) {
    Object.assign(state, initialState)
  }
}
