import { HTTP } from '../../../utils/http-common'

export default {
  state: {
    user: null,
    isLogged: false,
    successRegistering: false,
    successLogging: false,
    roles: null,
    isAdmin: false,
    token: null
  },
  mutations: {
    SET_USER (state, payload) {
      state.user = payload
    },
    SET_IS_LOGGED (state, payload) {
      state.isLogged = payload
    },
    SET_SUCCESS_REGISTERING (state, payload) {
      state.successRegistering = payload
    },
    SET_SUCCESS_LOGGING (state, payload) {
      state.successLogging = payload
    },
    SET_IS_ADMIN (state, payload) {
      state.isAdmin = payload
    },
    SET_TOKEN (state, payload) {
      state.token = payload
    }
  },
  getters: {
    user (state) {
      return state.user
    },
    successRegistering (state) {
      return state.successRegistering
    },
    successLogging (state) {
      return state.successLogging
    },
    isLogged (state) {
      return state.isLogged
    },
    roles (state) {
      return state.roles
    },
    isAdmin (state) {
      return state.isAdmin
    },
    token (state) {
      return state.token
    }
  },
  actions: {
    logout ({commit}) {
      this.commit('SET_USER', null)
      this.commit('SET_TOKEN', null)
    },
    login ({commit}, payload) {
      this.commit('SET_LOADING', true)
      let config = {
        auth: {
          username: payload.username,
          password: payload.password
        }
      }
      HTTP.post('/api/token', 'Getting Token', config)
        .then(res => {
          if (res.status === 200) {
            this.commit('SET_IS_LOGGED', true)
            this.commit('SET_LOADING', false)
            const newUser = {
              id: res.data.uid,
              name: res.data.name,
              surname: res.data.surname,
              username: res.data.username,
              email: res.data.email,
              photoUrl: res.data.photoUrl,
              roles: res.data.roles
            }
            this.commit('SET_USER', newUser)
            window.localStorage.setItem('userToken', res.data.token)
            window.localStorage.setItem('user', JSON.stringify(newUser))

            if (res.data.roles[0] === 'ROLE_ADMIN') {
              this.commit('SET_IS_ADMIN', true)
            }
          } else {
            this.commit('SET_IS_LOGGED', false)
          }
          this.commit('SET_LOADING', false)
          this.commit('SET_SUCCESS_LOGGING', true)
          payload.route.push('/home')
        })
        .catch(err => {
          this.commit('SET_LOADING', false)
          this.commit('SET_ERROR', err)
          console.log(err)
          console.log(err.message)
          console.log(err.statusCode)
          console.log(err.status)
        })
    },
    register ({commit}, payload) {
      this.commit('SET_LOADING', true)
      let user = {
        name: payload.name,
        surname: payload.surname,
        username: payload.username,
        email: payload.email,
        password: payload.password,
        photoUrl: payload.photoUrl
      }
      HTTP.post('/api/register', user)
        .then(res => {
          if (res.status === 200) {
            this.commit('SET_SUCCESS_REGISTERING', true)
            payload.route.push('/home')
          }
          this.commit('SET_LOADING', false)
        })
        .catch(err => {
          this.commit('SET_LOADING', false)
          this.commit('SET_ERROR', err)
          console.log(err)
        })
    },
    loginAuth ({commit}, payload) {
      const status = window.localStorage.getItem('userToken')
      if (status === null || status === undefined) {
        payload.route.push('/login')
      } else {
        payload.route.push('/free')
      }
    },
    registerAuth ({commit}, payload) {
      const status = window.localStorage.getItem('userToken')
      if (status === null || status === undefined) {
        payload.route.push('/register')
      } else {
        payload.route.push('/free')
      }
    },
    resetErrors ({commit}) {
      this.commit('SET_ERROR', null)
    },
    dismissRegisterSuccessAlert ({commit}) {
      this.commit('SET_SUCCESS_REGISTERING', false)
    },
    dismissLoginSuccessAlert ({commit}) {
      this.commit('SET_SUCCESS_LOGGING', false)
    }
  }
}
