import Home from '@/components/Home'
import LandingHome from '@/components/LandingHome'

import LoginView from '@/components/shared/LoginView'
import LogoutView from '@/components/shared/LogoutView'
import RegisterView from '@/components/shared/RegisterView'
import NotFoundView from '@/components/shared/NotFoundView'

import LandingCustomer from '@/components/customer/LandingCustomer'
import AccountView from '@/components/customer/AccountView'

import LandingSeller from '@/components/seller/LandingSeller'

import ProductsView from '@/components/products/ProductsView'

import ProtectedView from '@/components/demoPages/ProtectedView'
import FreeView from '@/components/demoPages/FreeView'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    children: [
      {
        path: '/protected',
        name: 'Protected',
        component: ProtectedView,
        meta: {description: 'Add new user', requiresAuth: true},
        beforeEnter: (to, from, next) => {
          console.log('Inside protected page')
          next()
        }
      },
      {
        path: '/free',
        name: 'Free',
        component: FreeView,
        beforeEnter: (to, from, next) => {
          console.log('Inside API protected page')
          next()
        }
      },
      {
        path: '/products',
        name: 'Products',
        component: ProductsView,
        beforeEnter: (to, from, next) => {
          console.log('Inside products page')
          next()
        }
      },
      {
        path: '/login', name: 'Login', component: LoginView
      },
      {
        path: '/logout', name: 'Logout', component: LogoutView
      },
      {
        path: '/register', name: 'Register', component: RegisterView
      },
      {
        path: '/home', name: 'LandingHome', component: LandingHome
      },
      {
        path: '/landingCustomer', name: 'LandingCustomer', component: LandingCustomer
      },
      {
        path: '/landingSeller', name: 'LandingSeller', component: LandingSeller
      },
      {
        path: '/account', name: 'Account', component: AccountView
      }
    ]
  },
  // not found handler
  {
    path: '*', component: NotFoundView
  }
]

export default routes
