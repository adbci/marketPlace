// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

// Import System requirements
import Vue from 'vue'
import VueRouter from 'vue-router'

import { sync } from 'vuex-router-sync'
import routes from './router/index'
import store from './store'
import {i18n} from './utils/i18n-setup'

// import internationalization support

// Import Helpers for filters
// import { domain, count, prettyDate, pluralize } from './filters'

// Import Views - Top level
import AppView from './App.vue'

// Import various modules
// import VueGoodTable from 'vue-good-table'
// import vSelect from 'vue-select'
// import Vuelidate from 'vuelidate'

// Import Install and register helper items
// Vue.filter('count', count)
// Vue.filter('domain', domain)
// Vue.filter('prettyDate', prettyDate)
// Vue.filter('pluralize', pluralize)

// Vue.component('v-select', vSelect)

// Vue.use declarations
Vue.use(VueRouter)

// Routing logic config
var router = new VueRouter({
  routes: routes,
  mode: 'history',
  linkExactActiveClass: 'active',
  scrollBehavior: function (to, from, savedPosition) {
    return savedPosition || {x: 0, y: 0}
  }
})

// Some middleware to help us ensure the user is authenticated.
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth) && (!router.app.$store.state.token || router.app.$store.state.token === 'null')) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    window.console.log('Not authenticated!')
    next({
      path: '/login',
      query: {redirect: to.fullPath}
    })
  } else {
    // verify token here

    next()
  }
})

sync(store, router)

// Check local storage to handle refreshes
if (window.localStorage) {
  let localUserString = window.localStorage.getItem('user') || 'null'
  let localUser = JSON.parse(localUserString)

  if (localUser && store.state.user !== localUser) {
    store.commit('SET_USER', localUser)
    store.commit('SET_TOKEN', window.localStorage.getItem('token'))
  }
}

// bootstrap declaration
require('../node_modules/bootstrap/scss/bootstrap.scss')

Vue.config.productionTip = false

// Start out app!
// eslint-disable-next-line no-new
new Vue({
  el: '#root',
  router: router,
  store: store,
  i18n,
  created () {},
  render: h => h(AppView)
})

// Hot updates
// if (module.hot) {
//   module.hot.accept(['./lang/en', './lang/fr'], function () {
//     i18n.setLocaleMessage('en', require('./lang/en').default)
//     i18n.setLocaleMessage('fr', require('./lang/fr').default)
//     // Or the following hot updates via $i18n property
//     // app.$i18n.setLocaleMessage('en', require('./en').default)
//     // app.$i18n.setLocaleMessage('ja', require('./ja').default)
//   })
// }
